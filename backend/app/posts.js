const express = require('express');
const Post = require('../models/Post');
const auth = require("../middleware/Auth");
const multer = require("multer");
const config = require("../config");
const path = require("path");
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const posts = await Post.find().populate('userId', 'username').sort({datetime: -1});
        res.send(posts);
    } catch (e) {
        res.status(500);
    }
});

router.get('/:id',async (req, res) => {
    try {
        const post = await Post.findById(req.params.id).populate('userId', 'username');

        if (post) {
            return res.send(post);
        } else {
            return res.status(404).send({error: 'Post not found'});
        }
    } catch (e) {
        return res.sendStatus(500);
    }
});

router.post('/', upload.single('image'), auth, async (req, res) => {
    if (!req.body.title) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const postData = {
        userId: req.user._id,
        title: req.body.title,
    };

    if (req.file) {
        postData.image = 'uploads/' + req.file.filename;
    }

    if (req.body.description) {
        postData.description = req.body.description;
    }

    if (!req.file) {
        if (!req.body.description) {
            return res.status(500).send({message: 'Description or Image fields must be filled!'});
        }
    }

    const post = new Post(postData);
    try {
        await post.save();
        res.send(post);

    } catch (e) {
        res.status(400).send({error: 'Data not valid'});
    }
});

router.delete('/:id', async (req, res) => {
   const post = await Post.findByIdAndDelete(req.params.id);
   res.send(post);
});
module.exports = router;