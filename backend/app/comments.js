const express = require('express');
const router = express.Router();
const Comment = require('../models/Comment');
const auth = require('../middleware/Auth');

router.get('/:postId', async (req, res) => {
    try {
        const comment = await Comment.find({postId: req.params.postId})
            .populate('userId', 'username');

        return res.send(comment);
    } catch (e) {
        return res.sendStatus(500);
    }
});

router.post('/', auth, async (req, res) => {
    if (!req.body.text || !req.body.postId) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const commentData = {
        userId: req.user._id,
        text: req.body.text,
        postId: req.body.postId,
    };

    const comment = new Comment(commentData);

    try {
        await comment.save();
        res.send(comment);
    } catch {
        res.status(400).send({error: 'Data not valid'});
    }
});

module.exports = router;