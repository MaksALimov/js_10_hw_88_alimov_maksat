const express = require('express');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const cors = require('cors');
const config = require('./config');
const users = require('./app/users');
const posts = require('./app/posts');
const comments = require('./app/comments');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));
const port = 8000;

app.use('/users', users);
app.use('/posts', posts);
app.use('/comments', comments);

const run = async () => {
    await mongoose.connect(config.db.url);

    app.listen(port, async () => {
        console.log(`Server Started on ${port} port!`);
    });

    exitHook(() => {
       console.log('Exiting');
       mongoose.disconnect();
    });
};

run().catch(e => console.error(e));
