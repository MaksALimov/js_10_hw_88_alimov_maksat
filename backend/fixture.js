const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require('./models/User');
const Post = require('./models/Post');
const Comment = require('./models/Comment');

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const collection of collections) {
        await mongoose.connection.db.dropCollection(collection.name);
    }

    const firstUser = await User.create({
        username: 'admin',
        password: '123',
        token: nanoid(),
    });

    const firstPost = await Post.create({
        userId: firstUser._id,
        title: 'Some Title',
        image: 'fixtures/avicii.jpg',
        datetime: new Date("11/20/2020 03:11"),
    });

    await Comment.create({
        userId: firstUser._id,
        postId: firstPost._id,
        text: 'Some comment',
    });

    await mongoose.connection.close();
};

run().catch(e => console.error(e));