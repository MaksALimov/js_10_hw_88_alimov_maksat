import React, {useState} from 'react';
import {Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {addPost} from "../../store/actions/postsActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(() => ({
    container: {
        marginTop: '80px',
    },

    inputsContainer: {
        width: '55%',
    },

    createPostButton: {
        fontWeight: 'bold',
    },

    notFound: {
        marginTop: '150px',
        textAlign: 'center',
        fontSize: '40px',
        fontWeight: 'bold',
    },
}));

const AddPost = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const addPostLoading = useSelector(state => state.posts.addPostLoading);
    const error = useSelector(state => state.posts.error);

    const [state, setState] = useState({
        title: "",
        description: "",
        image: null,
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const formSubmitHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        dispatch(addPost(formData));
    };

    return (
        <>
            {error &&
            <Grid item><Typography className={classes.notFound}>{error.message || error.global} </Typography></Grid>}
            <Grid container
                  component="form"
                  direction="column"
                  alignItems="center"
                  className={classes.container}
                  onSubmit={formSubmitHandler}
                  spacing={5}>
                <Grid item className={classes.inputsContainer}>
                    <FormElement
                        label="Title"
                        variant="outlined"
                        fullWidth
                        name="title"
                        value={state.title}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item className={classes.inputsContainer}>
                    <FormElement
                        label="Description"
                        variant="outlined"
                        multiline
                        fullWidth
                        name="description"
                        value={state.description}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item>
                    <TextField
                        type="file"
                        name="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>
                <Grid item>
                    <ButtonWithProgress
                        type="submit"
                        variant="contained"
                        className={classes.createPostButton}
                        loading={addPostLoading}
                        disabled={addPostLoading}
                    >
                        Create Post
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </>
    );
};

export default AddPost;