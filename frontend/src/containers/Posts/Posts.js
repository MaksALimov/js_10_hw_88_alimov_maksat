import React, {Fragment, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getPosts} from "../../store/actions/postsActions";
import Post from "../../components/Post/Post";
import {CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    container: {
      marginTop: '100px',
    },

    notFound: {
        textAlign: 'center',
        fontWeight: 'bold',
    },

    postsLoading: {
        textAlign: 'center',
    },
}));

const Posts = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts.posts);
    const postsLoading = useSelector(state => state.posts.postsLoading);
    const error = useSelector(state => state.posts.error);

    useEffect(() => {
        dispatch(getPosts());
    }, [dispatch]);

    return (
        <div className={classes.container}>
            {postsLoading ? <Grid item className={classes.postsLoading}><CircularProgress/></Grid> : error ?
                <Typography variant="h2" className={classes.notFound}>{error}</Typography> : posts.map(post => (
                    <Fragment key={post._id}>
                        <Post
                            username={post.userId.username}
                            title={post.title}
                            image={post.image}
                            datetime={post.datetime}
                            postId={post._id}
                        />
                    </Fragment>
                ))}
        </div>
    );
};

export default Posts;