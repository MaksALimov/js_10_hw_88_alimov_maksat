import React, {Fragment, useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getOnePost} from "../../store/actions/postsActions";
import {addComment, getPostComments} from "../../store/actions/commentsActions";
import {apiURL} from "../../config";
import FormElement from "../../components/UI/Form/FormElement";
import dayjs from "dayjs";
import Comment from "../../components/Comment/Comment";
import OnePostStyles from "./OnePostStyles";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const OnePost = ({match}) => {
    const classes = OnePostStyles();
    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.post);
    const user = useSelector(state => state.users.user);
    const error = useSelector(state => state.posts.error);
    const postLoading = useSelector(state => state.posts.postLoading);
    const comments = useSelector(state => state.comments.comments);
    const addCommentLoading = useSelector(state => state.comments.addCommentLoading);
    const commentsError = useSelector(state => state.comments.error);
    const addCommentError = useSelector(state => state.comments.addCommentError);

    const [comment, setComment] = useState({
        text: "",
        postId: match.params.id,
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setComment(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const formSubmitHandler = e => {
        e.preventDefault();
        dispatch(addComment(comment, match.params.id));
    };

    let image = null;

    if (post) {
        image = apiURL + '/' + post.image;
    }

    useEffect(() => {
        dispatch(getOnePost(match.params.id));
        dispatch(getPostComments(match.params.id));
    }, [dispatch, match.params.id]);

    return post && (
        <>
            {postLoading ? <Grid item className={classes.progress}><CircularProgress/></Grid> : error ?
                <Typography className={classes.notFound}>{error}</Typography> :
                <Grid container direction="column" alignItems="center" className={classes.container}>
                    <Grid item>
                        <Typography variant="h5" className={classes.username}>{post.userId.username}</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="h5" className={classes.title}>{post.title}</Typography>
                    </Grid>
                    {post.image ?
                        (<Grid item className={classes.imageContainer}>
                            <img src={image} alt="postPhoto" className={classes.image}/>
                        </Grid>) :
                        (<Grid item>
                            <Typography variant="h5" className={classes.description}>{post.description}</Typography>
                        </Grid>)}
                    <Grid item>
                        <Typography variant="h5">{dayjs(post.datetime).format('YYYY-MM-DD HH:mm')}</Typography>
                    </Grid>
                </Grid>}

            {comments.length > 0 ?
                (<Grid item className={classes.commentsTitleContainer}>
                    <Typography className={classes.commentsTitle}>Comments</Typography>
                </Grid>) : null}

            {commentsError ? <Typography className={classes.commentsError}>{commentsError}</Typography> : (
                comments.map(comment => (
                    <Fragment key={comment._id}>
                        <Comment
                            text={comment.text}
                            username={comment.userId.username}/>
                    </Fragment>
                )))}

            {user ? (
                <>
                    {addCommentError ? <Typography className={classes.notFound}>{addCommentError}</Typography> :
                        <Grid container
                              component="form"
                              direction="column"
                              alignItems="center"
                              onSubmit={formSubmitHandler}>

                            <Grid item>
                                <Typography className={classes.addCommentTitle}>Add Comment</Typography>
                            </Grid>
                            <Grid item>
                                <FormElement
                                    name="text"
                                    type="text"
                                    label="Text"
                                    variant="outlined"
                                    value={comment.text}
                                    required
                                    multiline
                                    onChange={inputChangeHandler}
                                />
                            </Grid>
                            <Grid item>
                                <ButtonWithProgress
                                    type="submit"
                                    variant="contained"
                                    className={classes.addCommentButton}
                                    loading={addCommentLoading}
                                    disabled={addCommentLoading}>
                                    Submit
                                </ButtonWithProgress>
                            </Grid>
                        </Grid>
                    }
                </>
            ) : null}
        </>
    );
};

export default OnePost;