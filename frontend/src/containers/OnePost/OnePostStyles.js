import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    container: {
        padding: '20px',
        margin: '20px 0',
        boxShadow: 'rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset',
        marginTop: '90px',
    },

    username: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        fontSize: '24px',
        marginBottom: '20px'
    },

    title: {
        textTransform: 'capitalize',
        fontSize: '24px',
        marginBottom: '10px',
    },

    description: {
        letterSpacing: '1px',
        wordSpacing: '1px',
        marginBottom: '15px',
    },

    imageContainer: {
        width: '250px',
        height: '250px',
        marginBottom: '20px',
    },

    image: {
        width: '100%',
        height: '100%',
    },

    notFound: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: '50px',
        marginTop: '70px',
    },

    progress: {
        textAlign: 'center',
    },

    commentsTitleContainer: {
        margin: '100px 0 40px',
    },

    commentsTitle: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: '40px',
    },

    addCommentTitle: {
        fontWeight: 'bold',
        fontSize: '35px',
        margin: '50px 0 30px',
    },

    addCommentButton: {
        margin: '40px 0',
        fontWeight: 'bold',
    },

    commentsErrorContainer: {
      marginTop: '100px',
    },

    commentsError: {
        fontWeight: 'bold',
        fontSize: '45px',
        margin: '50px 0 30px',
        textAlign: 'center',
    },
}));

export default useStyles;