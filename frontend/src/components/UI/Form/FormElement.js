import React from 'react';
import PropTypes from 'prop-types';
import {Grid, makeStyles, TextField} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    textFields: {
        margin: '5px 0',
    },
}))

const FormElement = ({label, name, value, onChange, required, error, autoComplete, type, multiline}) => {
    const classes = useStyles();
    return (
        <Grid item xs={12}>
            <TextField
                type={type}
                required={required}
                autoComplete={autoComplete}
                label={label}
                name={name}
                variant="outlined"
                fullWidth
                value={value}
                className={classes.textFields}
                onChange={onChange}
                error={Boolean(error)}
                helperText={error}
                multiline={multiline}
            />
        </Grid>
    );
};

FormElement.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    required: PropTypes.bool,
    error: PropTypes.string,
    autoComplete: PropTypes.string,
    type: PropTypes.string,
    multiline: PropTypes.bool,
};

export default FormElement;