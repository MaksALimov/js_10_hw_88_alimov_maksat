import React from 'react';
import {AppBar, Button, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import Anonymous from "../Anonymous/Anonymous";
import {logoutUser} from "../../../store/actions/usersActions";

const useStyles = makeStyles(() => ({
    appBar: {
        padding: '15px',
    },

    mainLink: {
        color: 'inherit',
        textDecoration: 'none',
    },

    buttons: {
        margin: '0 10px',
        boxShadow: 'rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px',
        fontWeight: 'bold',
    },

    username: {
        marginRight: '20px',
        fontSize: '24px',
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },

    usernameGreeting: {
        marginRight: '5px',
        fontSize: '17px',
    },

    addPostButton: {
        fontWeight: 'bold',
    },

    logoutButton: {
        fontWeight: 'bold',
        marginLeft: '20px',
    },
}));

const Header = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    return (
        <>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <Grid container justifyContent="space-between" alignItems="center">
                        <Typography variant="h3">
                            <Link to="/" className={classes.mainLink}>Forum</Link>
                        </Typography>
                        <Grid item>
                            {user ? (
                                <Grid container justifyContent="space-between" alignItems="center">
                                    <Grid item>
                                        <Typography className={classes.username}>
                                            <span
                                                className={classes.usernameGreeting}>Hello</span> {user.username + '!'}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Button
                                            component={Link}
                                            to="/add-post"
                                            variant="contained"
                                            className={classes.addPostButton}>
                                            Add Post
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button
                                            className={classes.logoutButton}
                                            variant="contained"
                                            onClick={() => dispatch(logoutUser())}
                                        >
                                            Logout
                                        </Button>
                                    </Grid>
                                </Grid>
                            ) : (
                                <Anonymous/>
                            )}
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar/>
        </>
    );
};

export default Header;