import React from 'react';
import {Link} from "react-router-dom";
import {Button, makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    buttons: {
        margin: '0 10px',
        boxShadow: 'rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px',
        fontWeight: 'bold',
    },
}));

const Anonymous = () => {
    const classes = useStyles();
    return (
        <>
            <Button
                variant="contained" className={classes.buttons}
                component={Link}
                to="/register">
                Register
            </Button>
            <Button
                variant="contained"
                component={Link}
                to="/login"
                className={classes.buttons}>
                Login
            </Button>
        </>
    );
};

export default Anonymous;