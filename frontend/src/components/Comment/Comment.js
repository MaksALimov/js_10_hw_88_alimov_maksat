import React from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    container: {
        boxShadow: 'rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px',
        padding: '20px',
        marginBottom: '60px',
    },

    username: {
        fontSize: '24px',
        fontWeight: 'bold',
    },

    commentText: {
        fontSize: '20px'
    },
}));

const Comment = ({text, username}) => {
    const classes = useStyles();
    return (
        <Grid container direction="column" alignItems="center" className={classes.container}>
            <Grid item>
                <Typography className={classes.username}>{username}</Typography>
            </Grid>
            <Grid item>
                <Typography className={classes.commentText}>{text}</Typography>
            </Grid>
        </Grid>
    );
};

export default Comment;