import React from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import dayjs from 'dayjs'
import {apiURL} from "../../config";
import ForumIcon from '@material-ui/icons/Forum';
import {Link} from "react-router-dom";

const useStyles = makeStyles(() => ({
    imgContainer: {
        width: '200px',
        height: '200px',
    },

    img: {
        width: '100%',
        height: '100%',
    },

    username: {
        textTransform: 'capitalize',
        fontSize: '26px',
    },

    datetime: {
        fontSize: '20px'
    },

    titleLink: {
        fontSize: '24px',
        textDecoration: 'none',
    },

    container: {
        padding: '20px',
        margin: '20px 0',
        boxShadow: 'rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset',
    },

    forumIcon: {
        width: '100%',
        height: '100%',
    },
}));

const Post = ({username, title, image, datetime, postId}) => {
    const classes = useStyles();
    const imgSrc = apiURL + '/' + image;

    return (
        <Grid container justifyContent="space-between" alignItems="center" spacing={3} className={classes.container}>
            <Grid item className={classes.imgContainer}>
                {image ? <img src={imgSrc} alt="img" className={classes.img}/> :
                    <ForumIcon className={classes.forumIcon}/>}
            </Grid>
            <Grid item>
                <Grid container justifyContent="space-evenly" alignItems="center">
                    <Grid item>
                        <Typography component="h2" className={classes.username}>{username}</Typography>
                    </Grid>
                    <Grid item>
                        <Typography className={classes.datetime}>
                            {dayjs(datetime).format('YYYY-MM-DD HH:mm')}
                        </Typography>
                    </Grid>
                    <Grid container direction="column" alignItems="center">
                        <Grid item>
                            <Link
                                to={`post/${postId}`}
                                className={classes.titleLink}>
                                {title}
                            </Link>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Post;