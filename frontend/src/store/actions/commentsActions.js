import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";

export const GET_POST_COMMENTS_REQUEST = 'GET_POST_COMMENTS_REQUEST';
export const GET_POST_COMMENTS_SUCCESS = 'GET_POST_COMMENTS_SUCCESS';
export const GET_POST_COMMENTS_FAILURE = 'GET_POST_COMMENTS_FAILURE';

export const ADD_COMMENT_REQUEST = 'ADD_COMMENT_REQUEST';
export const ADD_COMMENT_SUCCESS = 'ADD_COMMENT_SUCCESS';
export const ADD_COMMENT_FAILURE = 'ADD_COMMENT_FAILURE';

export const getPostCommentsRequest = () => ({type: GET_POST_COMMENTS_REQUEST});
export const getPostCommentsSuccess = comments => ({type: GET_POST_COMMENTS_SUCCESS, payload: comments});
export const getPostCommentsFailure = error => ({type: GET_POST_COMMENTS_FAILURE, payload: error});

export const addCommentRequest = () => ({type: ADD_COMMENT_REQUEST});
export const addCommentSuccess = comment => ({type: ADD_COMMENT_SUCCESS, payload: comment});
export const addCommentFailure = error => ({type: ADD_COMMENT_FAILURE, payload: error});

export const getPostComments = postId => {
    return async dispatch => {
        try {
            dispatch(getPostCommentsRequest());

            const response = await axiosApi.get(`/comments/${postId}`);
            dispatch(getPostCommentsSuccess(response.data));

            if (response.data.length > 0) {
                toast.success('Comments loaded!', {
                    autoClose: 2000,
                });
            }

        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(getPostCommentsFailure(error.response.statusText));
                toast.error('Could not load comments');
            } else {
                dispatch(getPostCommentsFailure(error.message));
                toast.error('Could not load comments');
            }
        }
    }
};

export const addComment = (commentData, postId) => {
    return async (dispatch, getState) => {
        try {
            dispatch(addCommentRequest());

            const headers = {
                'Authorization': getState().users.user && getState().users.user.token,
            };

            const response = await axiosApi.post(`/comments`, commentData, {headers});
            dispatch(addCommentSuccess(response.data));
            dispatch(getPostComments(postId))
            toast.success('Commend added!', {
                autoClose: 2000,
            });
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(addCommentFailure(error.response.statusText));
                toast.error('Could not add comment. Make sure that the path is correct');
            } else {
                dispatch(addCommentFailure(error.message));
            }
        }
    };
};