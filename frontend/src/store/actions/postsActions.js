import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./historyActions";

export const GET_POSTS_REQUEST = 'GET_POSTS_REQUEST';
export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS';
export const GET_POSTS_FAILURE = 'GET_POSTS_FAILURE';

export const GET_ONE_POST_REQUEST = 'GET_ONE_POST_REQUEST';
export const GET_ONE_POST_SUCCESS = 'GET_ONE_POST_SUCCESS';
export const GET_ONE_POST_FAILURE = 'GET_ONE_POST_FAILURE';

export const ADD_POST_REQUEST = 'ADD_POST_REQUEST';
export const ADD_POST_SUCCESS = 'ADD_POST_SUCCESS';
export const ADD_POST_FAILURE = 'ADD_POST_FAILURE';

export const getPostsRequest = () => ({type: GET_POSTS_REQUEST});
export const getPostsSuccess = posts => ({type: GET_POSTS_SUCCESS, payload: posts});
export const getPostsFailure = error => ({type: GET_POSTS_FAILURE, payload: error});

export const getOnePostRequest = () => ({type: GET_ONE_POST_REQUEST});
export const getOnePostSuccess = post => ({type: GET_ONE_POST_SUCCESS, payload: post});
export const getOnePostFailure = error => ({type: GET_ONE_POST_FAILURE, payload: error});

export const addPostRequest = () => ({type: ADD_POST_REQUEST});
export const addPostSuccess = post => ({type: ADD_POST_SUCCESS, payload: post});
export const addPostFailure = error => ({type: ADD_POST_FAILURE, payload: error});

export const getPosts = () => {
    return async dispatch => {
        try {
            dispatch(getPostsRequest());

            const response = await axiosApi.get('/posts');
            dispatch(getPostsSuccess(response.data));

            if (response.data.length === 0) {
                toast.warning('There is no posts');
            } else {
                toast.success('Posts loaded!', {
                    autoClose: 2500,
                });
            }

        } catch (error) {
            if (!error.response) {
                return dispatch(getPostsFailure(error.message));
            }

            if (error.response.status === 404) {
                dispatch(getPostsFailure(error.response.statusText));
                toast.error('Posts not found');
            } else {
                toast.error('Could not load posts');
            }
        }
    };
};

export const getOnePost = postId => {
    return async dispatch => {
        try {
            dispatch(getOnePostRequest());

            const response = await axiosApi.get(`/posts/${postId}`);
            dispatch(getOnePostSuccess(response.data));
            toast.success('Post loaded!', {
                autoClose: 2500,
            });
        } catch (error) {
            if (!error.response) {
                return dispatch(getOnePostFailure(error.message));
            }

            if (error.response.status === 404) {
                dispatch(getOnePostFailure(error.response.statusText));
                toast.error('Post not found');
            } else {
                toast.error('Could not load post');
            }
        }
    };
};

export const addPost = postData => {
    return async (dispatch, getState) => {
        try {
            dispatch(addPostRequest());

            const headers = {
                'Authorization': getState().users.user && getState().users.user.token,
            };

            const response = await axiosApi.post('/posts', postData, {headers});
            dispatch(addPostSuccess(response.data));
            dispatch(historyPush('/'));
            toast.success('Post created', {
                autoClose: 3000,
            });
        } catch (error) {

            if (error.response && error.response.data) {
                return dispatch(addPostFailure(error.response.data));
            } else {
               return dispatch(addPostFailure({global: 'No Internet'}))
            }
        }
    };
};
