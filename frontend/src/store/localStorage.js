export const saveToLocalStorage = state => {
    try {
        const serialized = JSON.stringify(state);
        localStorage.setItem('state', serialized);

    } catch (e) {
        console.error('Could not save state');
    }
};

export const loadFromLocalStorage = () => {
    try {
        const serialized = localStorage.getItem('state');

        if (serialized === null) {
            return undefined;
        }

        return JSON.parse(serialized);
    } catch (e) {
        return undefined;
    }
};