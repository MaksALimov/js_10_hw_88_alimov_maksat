import {
    ADD_COMMENT_FAILURE,
    ADD_COMMENT_REQUEST,
    ADD_COMMENT_SUCCESS, GET_POST_COMMENTS_FAILURE,
    GET_POST_COMMENTS_REQUEST, GET_POST_COMMENTS_SUCCESS
} from "../actions/commentsActions";

const initialState = {
    comment: null,
    comments: [],
    error: null,
    addCommentLoading: false,
    getCommentsLoading: false,
    addCommentError: false,
};

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_COMMENT_REQUEST:
            return {...state, addCommentLoading: true};

        case ADD_COMMENT_SUCCESS:
            return {...state, comment: action.payload, addCommentLoading: false};

        case ADD_COMMENT_FAILURE:
            return {...state, addCommentError: action.payload, addCommentLoading: false};

        case GET_POST_COMMENTS_REQUEST:
            return {...state, getCommentsLoading: true};

        case GET_POST_COMMENTS_SUCCESS:
            return {...state, comments: action.payload, getCommentsLoading: false};

        case GET_POST_COMMENTS_FAILURE:
            return {...state, error: action.payload, getCommentsLoading: false};

        default:
            return state;
    }
};

export default commentsReducer;