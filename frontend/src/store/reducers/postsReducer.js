import {
    ADD_POST_FAILURE,
    ADD_POST_REQUEST, ADD_POST_SUCCESS,
    GET_ONE_POST_FAILURE,
    GET_ONE_POST_REQUEST,
    GET_ONE_POST_SUCCESS,
    GET_POSTS_FAILURE,
    GET_POSTS_REQUEST,
    GET_POSTS_SUCCESS
} from "../actions/postsActions";

const initialState = {
    postsLoading: null,
    posts: [],
    post: null,
    error: null,
    postLoading: null,
    addPostLoading: null,
};

const postsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_POSTS_REQUEST:
            return {...state, postsLoading: true};

        case GET_POSTS_SUCCESS:
            return {...state, posts: action.payload, postsLoading: false, error: null};

        case GET_POSTS_FAILURE:
            return {...state, error: action.payload, postsLoading: false};

        case GET_ONE_POST_REQUEST:
            return {...state, postLoading: true};

        case GET_ONE_POST_SUCCESS:
            return {...state, post: action.payload, postLoading: false};

        case GET_ONE_POST_FAILURE:
            return {...state, post: action.payload, error: action.payload, postLoading: false};

        case ADD_POST_REQUEST:
            return {...state, addPostLoading: true, error: null};

        case ADD_POST_SUCCESS:
            return {...state, post: action.payload, addPostLoading: false, error: null};

        case ADD_POST_FAILURE:
            return {...state, error: action.payload, addPostLoading: false};

        default:
            return state;
    }
};

export default postsReducer;