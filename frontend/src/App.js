import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Posts from "./containers/Posts/Posts";
import OnePost from "./containers/OnePost/OnePost";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddPost from "./containers/AddPost/AddPost";
const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Posts}/>
            <Route path="/post/:id" component={OnePost}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
            <Route path="/add-post" component={AddPost}/>
        </Switch>
    </Layout>
);

export default App;
